
const fs = require('fs');
const http = require('http');
const wifi = require("node-wifi");

const url_config_file  = 'http://192.168.43.1:8080/mtbox-config.json';
const  config_file = '/home/marabout/mtbox-config.json';
const ssid_wifi = "AndroidAP2281";
const password_wifi = "azertyaz";
const wifi_interface = null;

const download = (url, dest, cb) => {
    const file = fs.createWriteStream(dest);
    const request = http.get(url, (response) => {
        // check if response is success
        if (response.statusCode !== 200) {
            return cb('Response status was ' + response.statusCode);
        }

        response.pipe(file);
    });

    // close() is async, call cb after close completes
    file.on('finish', () => file.close(cb));

    // check for request error too
    request.on('error', (err) => {
        console.log('error')
        fs.unlink(dest);
        return cb(err.message);
    });

    file.on('error', (err) => { // Handle errors
        fs.unlink(dest); // Delete the file async. (But we don't check the result)
        return cb(err.message);
    });
};


wifi.init({
  iface: wifi_interface,
});

// Connect to a basic Network
wifi.connect({ ssid: ssid_wifi, password: password_wifi}, function(err) {
	if (err) {
		console.log("err", err);
	}
	console.log("Connected");

	// download  the  file
	download(url_config_file, config_file, (err_download)  => {
		if (err_download) {
			console.log('error download  file', err_download);
		} else {
        		console.log('file downloaded  with success');
		}
	});

});

